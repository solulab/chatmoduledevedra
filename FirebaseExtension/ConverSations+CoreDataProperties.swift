//
//  ConverSations+CoreDataProperties.swift
//  
//
//  Created by Admin on 03/10/18.
//
//

import Foundation
import CoreData


extension ConverSations {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ConverSations> {
        return NSFetchRequest<ConverSations>(entityName: "ConverSations")
    }

    @NSManaged public var convesation_ID: Int64
    @NSManaged public var last_message: NSObject?
    @NSManaged public var messageID: String?
    @NSManaged public var is_group: String?
    @NSManaged public var timestamp: String?
    @NSManaged public var typing: String?
    @NSManaged public var chathistory: NSObject?

}
