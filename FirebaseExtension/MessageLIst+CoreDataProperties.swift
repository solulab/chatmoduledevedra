//
//  MessageLIst+CoreDataProperties.swift
//  
//
//  Created by Admin on 03/10/18.
//
//

import Foundation
import CoreData


extension MessageLIst {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MessageLIst> {
        return NSFetchRequest<MessageLIst>(entityName: "MessageLIst")
    }

    @NSManaged public var mediaURL: String?
    @NSManaged public var message: String?
    @NSManaged public var message_id: Int64
    @NSManaged public var message_status: NSObject?
    @NSManaged public var sender_name: String?
    @NSManaged public var text: String?
    @NSManaged public var time: String?
    @NSManaged public var type: String?
    @NSManaged public var user: String?

}
