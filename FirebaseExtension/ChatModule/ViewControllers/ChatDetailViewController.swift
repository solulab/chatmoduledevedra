//
//  ChatDetailViewController.swift
//  HelloLayover
//
//  Created by User on 8/21/17.
//  Copyright © 2017 SoluLab. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import AWSS3
import AVFoundation
import AVKit
import SDWebImage
import IQKeyboardManagerSwift
import MobileCoreServices
import AVFoundation
import AVKit
import AWSCognito
import AWSCore
import Kingfisher

let SCREEN_SIZE = UIScreen.main.bounds.size
let AWS_BUCKET_NAME = "finalchatsolulab"// "solulabclone"
let AWS_BUCKET_URL = "https://s3.ap-south-1.amazonaws.com/finalchatsolulab/"

enum MessageTypes : String
{
    case PHOTO = "Photo"
    case VIDEO = "Video"
    case GROUP_REQUEST = "Group_Request"
}

class ChatDetailViewController: BaseViewController
{
    @IBOutlet var chatViewBottom: NSLayoutConstraint!
    @IBOutlet weak var tblView : UITableView!
    @IBOutlet var txtViewHeight: NSLayoutConstraint!
    @IBOutlet var txtView : UITextView!
    @IBOutlet var btnSendMessage : UIButton!
    @IBOutlet var lblUsername : UILabel!
    @IBOutlet var imgDp : UIImageView!
    @IBOutlet var messageView : UIView!
    @IBOutlet var lblStatus : UILabel!
    
    @IBOutlet var bgViewWidth: NSLayoutConstraint!
    var tableReloadTimer: Timer?
    
    var isOpen : Bool = false
    var isGroupChat : Bool = false
    
    var textViewPlaceholder = "Type a Message"
    
    var mediaType : String = ""
    var mediaURL : String = ""
    var mediaStr = "Media"
    let minChatBGViewWidth : CGFloat = 75.0
    let maxChatBGViewWidth = SCREEN_SIZE.width - 56.5
    var sectionsInTable = [String]()
    var valueChangeRef : DatabaseReference?
    
    // var chatCellDelegate : ChatUserCellProtocol?
    var counterrr = 0
    var idForDetail = ""
    var isAvPlayerPresented : Bool = false
    
    var tempSections = [String]()
    var isTableReloaded = false
    var txtTemp : UITextView?
    var isTyping = false
    var imgPicker = UIImagePickerController()
    
    var conversationObj = Conversation()
    var messages = [Message]()
    var chathistoryTime = ""
    var sectionData = [String:[Message]]()
    
    let AWSNEW_IDENTITY_POOL_ID = "ap-south-1:341684dd-c9fe-4459-ab31-93b353c6dc09"
    
    var sortedKeys = [String]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.txtView.delegate = self
        txtViewHeight.constant = 40
        
        
        if let dict = conversationObj.chatHistoryDictionary{
            for (key,value) in dict
            {
                if (key as! String) == kcurrentUSERID{
                    chathistoryTime = value as! String
                }
                
            }
        }
        initializeMessageRef()
        fetchMessages(chatHistoryTime: self.chathistoryTime)
        
        if txtView.text.trim() == textViewPlaceholder{
            btnSendMessage.isEnabled = false
        }
        imgPicker.delegate = self
        
        lblStatus.text = ""
        self.tblView.showsVerticalScrollIndicator = false
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil
        )
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: NSNotification.Name.UIKeyboardWillHide,
            object: nil
        )
        
        
        if conversationObj.isGroup{
            displayGroupDetail(conversationID: self.conversationObj.conversationID)
        }else{
            for(key,_) in self.conversationObj.userDictionary!
            {
                if (key as! String) != kcurrentUSERID{
                    displayUserDetail(userID: (key as! String))
                }
            }
        }
        
        
        addTypingObserver()
        setAWSCredentials()
        // setNewAWSCredentials()
        
    }
    
    func initializeMessageRef()
    {
        //if messagesRef == nil{
        messagesRef = Database.database().reference().child("messages")
        //}
        messagesRef.keepSynced(true)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        IQKeyboardManager.sharedManager().enable = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(uploadDownloadProgressChanged(notification:)), name: AWSNotificationProgress, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(uploadDownloadFailed(notification:)), name: AWSNotificationFailed, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification)
    {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            self.chatViewBottom.constant = keyboardHeight
            self.view.layoutIfNeeded()
            // self.tblView.reloadData()
            //self.scrollToBottom()
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        
        self.chatViewBottom.constant = 0
        //self.tblView.layoutIfNeeded()
        self.view.layoutIfNeeded()
        //self.scrollToBottom()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        if self.conversationRef != nil{
            changeStatusinDB(isTyping: false)
        }
        // IQKeyboardManager.sharedManager().enable = true
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Functions -
    
    @objc func reloadTableAndScrollToBottom()
    {
        self.tblView.reloadData()
        self.scrollToBottom()
    }
    
    @objc func uploadDownloadProgressChanged(notification: Notification)
    {
        if let notiObject = notification.object as? [String : Any]
        {
            let mID = notiObject["messageId"] as! String
            let progressValue = notiObject["Progress"] as! CGFloat
            
            let cellArray = tblView.visibleCells
            let mediaCells = cellArray.filter { (cell) -> Bool in
                
                if let med = cell as? MediaCell
                {
                    return med.messageID == mID
                }
                return false
            }
            
            if mediaCells.count > 0
            {
                let cell = mediaCells[0] as! MediaCell
                
                DispatchQueue.main.async
                    {
                        if !cell.progressView.isProgressing
                        {
                            cell.progressView.startProgressing()
                            
                        }
                        
                        if progressValue == 1{
                            cell.btnVideoAction.isHidden = false
                            cell.progressView.isHidden = true
                            cell.btnRetryAction.isHidden = true
                        }else
                        {
                            cell.btnVideoAction.isHidden = true
                            cell.btnRetryAction.isHidden = true
                            cell.progressView.isHidden = false
                        }
                        cell.progressView.setProgress(progressValue, animated: true)
                }
            }
        }
    }
    
    @objc func uploadDownloadFailed(notification: Notification)
    {
        if let notiObject = notification.object as? [String : Any]
        {
            let mID = notiObject["messageId"] as! String
            let progressValue = notiObject["Error"] as! String
            
            print("Error in object: \(notiObject)")
            
            Helper.displayAlert(title: progressValue, msg: "")
            //self.showToast(msg: progressValue)
            
            
            let cellArray = tblView.visibleCells
            let mediaCells = cellArray.filter { (cell) -> Bool in
                
                if let med = cell as? MediaCell
                {
                    return med.messageID == mID
                }
                return false
            }
            
            if mediaCells.count > 0
            {
                let cell = mediaCells[0] as! MediaCell
                DispatchQueue.main.async {
                    cell.btnRetryAction.isHidden = false
                    cell.progressView.isHidden = true
                }
                
            }
        }
    }
    
    func uploadVideoToAWS(videoURL : URL,fileName : String)
    {
        let key = self.messagesRef!.childByAutoId().key
        
        let messageRef = self.messagesRef.child(self.conversationObj.conversationID).child(key)
        
        if AWSUtilityManager.shared.uploadFileWith(fileName, fileURL: videoURL, key: key, messageRef: messageRef) == false
        {
            // File not found
        }
        self.mediaURL = ""
        self.addMessageToDB(isMedia: true, messageID: key)
        DispatchQueue.main.async(execute: {() -> Void in
            self.tblView.reloadData()
        })
        
    }
    
    func encodeVideo(videoURL: URL,fileName : String)
    {
        let avAsset = AVURLAsset(url: videoURL)
        let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough)
        
        
        
        let docDir2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as NSURL
        
        let filePath = docDir2.appendingPathComponent(fileName)
        deleteFile(filePath!)
        
        
        
        exportSession?.outputURL = filePath
        exportSession?.outputFileType = AVFileType.mp4
        exportSession?.shouldOptimizeForNetworkUse = true
        
        let start = CMTimeMakeWithSeconds(0.0, 0)
        let range = CMTimeRange(start: start, duration: avAsset.duration)
        exportSession?.timeRange = range
        
        exportSession!.exportAsynchronously{() -> Void in
            switch exportSession!.status
            {
            case .failed:
                // spinnerObj?.hideCustomLoader()
                print("\(exportSession!.error!)")
            case .cancelled:
                //spinnerObj?.hideCustomLoader()
                print("Export cancelled")
            case .completed:
                
                print("Successful")
                if let newURL = exportSession?.outputURL
                {
                    self.uploadVideoToAWS(videoURL: newURL,fileName: fileName)
                    //                    self.imageURL = newURL as NSURL
                    //                    self.uploadImage()
                }
                
            default:
                break
            }
            
        }
    }
    
    func changeStatusinDB(isTyping : Bool)
    {
        self.conversationRef.child(self.conversationObj.conversationID).child("Typing").child(kcurrentUSERID).setValue(isTyping ? "true" : "false")
    }
    
    func scrollToBottom()
    {
        let lastSection = self.sectionData.keys.count - 1
        
        if lastSection >= 0
        {
            let tdata =  self.sectionData[self.sortedKeys[lastSection]]
            
            if tdata!.count > 0
            {
                self.tblView.scrollToRow(at: IndexPath(row: tdata!.count - 1, section: lastSection), at: .bottom, animated: true)
            }
        }
    }
    
    func clearChat()
    {
        self.conversationRef.child(self.conversationObj.conversationID).child("chatHistory").child(kcurrentUSERID).setValue(Int64(Date().timeIntervalSince1970 * 1000).description)
        self.messages.removeAll()
        self.tblView.reloadData()
    }
    
    //uploading image to FirebaseDB and will get Firebase image url from this function
    func uploadMedia(img : UIImage, completion: @escaping (_ url: String?) -> Void)
    {
        let storageRef = Storage.storage().reference().child("images/\(Date().timeIntervalSince1970)")
        if let uploadData = UIImageJPEGRepresentation(img, 0.25)
        {
            storageRef.putData(uploadData, metadata: nil, completion: { (metaData, error)
                in
                
                if error != nil {
                    print("error")
                    completion(nil)
                } else {
                    completion((metaData?.downloadURL()?.absoluteString)!)
                    
                    // your uploaded photo url.
                }
            })
        }
    }
    
    func uploadImageToFirebase(img : UIImage)
    {
        uploadMedia(img : img)
        { url in
            if url != nil
            {
                self.mediaURL = url!
                self.addMessageToDB(isMedia: true,messageID: "")
            }
        }
    }
    
    func displayDataInMediaCell(cell : MediaCell,data : Message)
    {
        cell.clearData()
        cell.btnVideoAction.isHidden = true
        cell.mediaImgView.image = UIImage()
        cell.mediaImgView.backgroundColor = UIColor.clear
        cell.mediaImgView.removeImageViewer()
        cell.mediaURL = data.mediaURL
        
        if(cell.spinnerView != nil )
        {
            cell.spinnerView.isHidden = false
            cell.spinnerView.setAnimating(true)
            cell.spinnerView.lineWidth = 5.0
            cell.spinnerView.tintColor = UIColor.red
            cell.spinnerView.backgroundColor = UIColor.clear
        }
        
        if conversationObj.isGroup && cell.lblNameHeight != nil
        {
            cell.lblNameHeight.constant = 25
            cell.lblUserName.text = data.senderName
        }else{
            if cell.lblNameHeight != nil{
                cell.lblNameHeight.constant = 0
            }
        }
        
        if data.mediaURL != ""
        {
            if data.messageType == MessageTypes.VIDEO.rawValue
            {
                if data.videoThumbnail != ""
                {
                    
                    let url = URL(string:"\(AWS_BUCKET_URL)thumbnail/\(data.videoThumbnail)")
                    cell.mediaImgView.kf.setImage(with: url,
                                                  placeholder: nil,
                                                  options: [.transition(ImageTransition.fade(1))],
                                                  progressBlock: { receivedSize, totalSize in
                                                    print("Progress : \(AWS_BUCKET_URL)thumbnail/\(data.videoThumbnail)")
                                                    
                    },
                                                  completionHandler: { image, error, cacheType, imageURL in
                                                    cell.mediaImgView.setupImageViewer()
                                                    
                                                    print("Done : - \(AWS_BUCKET_URL)thumbnail/\(data.videoThumbnail)")
                                                    

                    })
                    
                    
                    
                    
                    //                        cell.mediaImgView.kf.setImage(with: url)
                    // cell.mediaImgView.
                    // sd_setImage(with: URL(string:"\(AWS_BUCKET_URL)thumbnail/\(data.videoThumbnail)"), placeholderImage: nil, options: .highPriority, completed: nil)
                    
                    // cell.mediaImgView.sd_setImage(with: URL(string:"\(AWS_BUCKET_URL)thumbnail/\(data.videoThumbnail)"), placeholderImage: nil, options: .highPriority, completed: nil)
                    
                }
                
                if !cell.isDownloadCell{
                    cell.btnVideoAction.isHidden = false
                    if(cell.spinnerView != nil)
                    {
                        cell.spinnerView.isHidden = true
                    }
                    cell.mediaImgView.setupImageViewer()
                    return
                }
                
                if Helper.isFileExistInDevice(nameOfFile: data.mediaURL)
                {
                    cell.btnVideoAction.isHidden = false
                    cell.btnRetryAction.isHidden = true
                }else{
                    cell.btnRetryAction.isHidden = false
                    cell.btnVideoAction.isHidden = true
                }
                if(cell.spinnerView != nil)
                {
                    cell.spinnerView.isHidden = true
                }
                cell.mediaImgView.setupImageViewer()
                
            }
            else
            {
                cell.btnVideoAction.isHidden = true
                if(cell.spinnerView != nil )
                {
                    cell.spinnerView.isHidden = false
                    cell.spinnerView.setAnimating(true)
                    cell.spinnerView.lineWidth = 5.0
                    cell.spinnerView.tintColor = UIColor.red
                    cell.spinnerView.backgroundColor = UIColor.clear
                }
                
                cell.mediaImgView.sd_setImage(with: URL(string:data.mediaURL), placeholderImage: nil, options: SDWebImageOptions.highPriority, completed:
                    { (img, error, cacheType, url)
                        in
                        
                        if(cell.spinnerView != nil)
                        {
                            cell.spinnerView.isHidden = true
                        }
                        cell.mediaImgView.setupImageViewer()
                })
            }
        }
        else
        {
            cell.mediaImgView.image = UIImage()
            cell.mediaImgView.backgroundColor = UIColor.clear
            if(cell.spinnerView != nil)
            {
                cell.spinnerView.isHidden = true
            }
            cell.mediaImgView.setupImageViewer()
            
        }
    }
    
    func displayDataInTextCell(cell : ChatUserCell,data : Message)
    {
        if conversationObj.isGroup{
            let combination = NSMutableAttributedString()
            
            if cell.accessibilityIdentifier == "ReceiverCell" || cell.accessibilityIdentifier == "ReceiverMediaCell"
            {
                combination.append(getConfiguredAttributedStringName(text: "\(data.senderName)\n\n"))
                
                combination.append(getConfiguredAttributedString(text: "\(data.messageText)"))
                cell.txtviewMessage.attributedText = combination
            }else
            {
                cell.txtviewMessage.attributedText = getConfiguredAttributedString(text: data.messageText)
            }
        }else{
            cell.txtviewMessage.attributedText = getConfiguredAttributedString(
                text: data.messageText)
        }
        
    }
    
    func refreshStatus()
    {
        var statusChanged = false
        if let dict = conversationObj.typingDictionary{
            for(key,value) in dict
            {
                if String(describing: value) == "true" && String(describing: key) != kcurrentUSERID
                {
                    statusChanged = true
                    if conversationObj.isGroup
                    {
                        
                        delegate?.getUserNameForUserID((key as! String), completion: { (userObj) in
                            self.lblStatus.text = "\(userObj.name) is typing..."
                        })
                        
                        
                        
                    }else
                    {
                        self.lblStatus.text = "typing..."
                    }
                }
            }
        }
        
        if !statusChanged
        {
            lblStatus.text = ""
        }
        
    }
    
    func addTypingObserver()
    {
        self.conversationRef.child(self.conversationObj.conversationID).child("Typing").removeAllObservers()
        self.conversationRef.child(self.conversationObj.conversationID).child("Typing").observe(DataEventType.value, with: { (snapshot) in
            let dict = snapshot.value as! Dictionary<String,Any>
            self.conversationObj.typingDictionary = NSMutableDictionary(dictionary: dict as NSDictionary)
            
            if showTypingIndicator{
                self.refreshStatus()
            }
            
        })
    }
    
    func displayGroupDetail(conversationID : String?)
    {
        delegate?.getGroupNameForConvID(conversationID, completion: { (groupObj) in
            self.lblUsername.text = groupObj.name
            self.imgDp.sd_setImage(with: URL(string:groupObj.imageURL), completed: nil)
        })
    }
    
    func displayUserDetail(userID : String?)
    {
        delegate?.getUserNameForUserID(userID, completion: { (userObj) in
            self.lblUsername.text = userObj.name
            self.imgDp.sd_setImage(with: URL(string:userObj.imageURL), completed: nil)
        })
    }
    
    //Fetched all messages from messages table of perticular conversation Ex : (Aabid - Kunal chat)
    func fetchMessages(chatHistoryTime : String)
    {
        let dbRef = messagesRef.child(self.conversationObj.conversationID).queryOrdered(byChild: "time").queryStarting(atValue: chatHistoryTime)
        var tObj = [Message]()
        dbRef.observe(DataEventType.childAdded, with: { (snapshot) in
            
            //if the reference have some values
            if snapshot.childrenCount > 0
            {
                let obj = Message()
                obj.update(data: snapshot.value as! Dictionary<String,Any>)
                obj.id = snapshot.key
                
                let dateStr = Helper.getFormattedDateTime(secondss: Double(obj.timestamp)!, isChatDetailScreen: true)
                
                if self.sectionData[dateStr] == nil
                {
                    self.sortedKeys.append(dateStr)
                    tObj = [Message]()
                }
                tObj.append(obj)
                self.sectionData[dateStr] = tObj
                self.messages.append(obj)
            }
            
            if self.tableReloadTimer != nil && self.tableReloadTimer?.isValid == true
            {
                self.tableReloadTimer?.invalidate()
            }
            self.tableReloadTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.reloadTableAndScrollToBottom), userInfo: nil, repeats: false)
            
            if self.valueChangeRef == nil{
                self.addObserverForValueChange(chatHistoryTime: chatHistoryTime)
            }
        })
    }
    
    func addObserverForValueChange(chatHistoryTime : String)
    {
        self.valueChangeRef = Database.database().reference().child("messages").child(self.conversationObj.conversationID)
        let dbRef = self.valueChangeRef?.queryOrdered(byChild: "time").queryStarting(atValue: chatHistoryTime)
        
        dbRef?.observe(.childChanged, with: { (snapshot) in
            let dict = snapshot.value as! Dictionary<String,Any>
            let obj = Message()
            obj.update(data: dict)
            obj.id = snapshot.key
            
            if let index = self.messages.index(where: {$0.id == obj.id}) {
                self.messages.remove(at: index)
                self.messages.insert(obj, at: index)
            }
            self.tblView.reloadData()
        })
    }
    
    func addMessageToDB(isMedia : Bool,messageID : String)
    {
        let key = messageID.count > 0 ? messageID : self.messagesRef!.childByAutoId().key
        
        let msgObj = Message()
        msgObj.messageText = self.txtView.text.trim()
        msgObj.senderName = kcurrenStaticName
        msgObj.timestamp =  Int64(Date().timeIntervalSince1970 * 1000).description
        
        
        
        let msgDict = NSMutableDictionary()
        
        msgDict.setValue(msgObj.messageText, forKey: "message")
        msgDict.setValue(msgObj.timestamp, forKey: "time")
        msgDict.setValue(kcurrentUSERID, forKey: "user")
        msgDict.setValue("type", forKey: msgObj.messageType)
        
        if isMedia
        {
            msgObj.messageType = mediaStr
            msgObj.mediaURL = self.mediaURL
            msgObj.messageText = mediaStr
        }
        
        msgDict.setValue(msgObj.messageType, forKey: "type")
        msgDict.setValue(msgObj.mediaURL, forKey: "mediaURL")
        
        let dict = NSMutableDictionary()
        
        for(key,_) in conversationObj.userDictionary!
        {
            if (key as! String) == kcurrentUSERID{
                dict.setValue("r", forKey: key as! String)
            }else{
                dict.setValue("s", forKey: key as! String)
            }
        }
        
        msgDict.setValue(dict, forKey: "message_status")
        msgDict.setValue(msgObj.senderName, forKey: "senderName")
        
        messagesRef.child(conversationObj.conversationID).child(key).setValue(msgDict)
        
        let dbConversationRef = Database.database().reference().child("conversations").child(self.conversationObj.conversationID)
        
        let conversationDict = NSMutableDictionary()
        conversationDict.setValue(conversationObj.conversationID, forKey: "conversationID")
        conversationDict.setValue(msgObj.messageText, forKey: "text")
        conversationDict.setValue(msgObj.timestamp, forKey: "timestamp")
        conversationDict.setValue(msgObj.messageType, forKey: "type")
        conversationDict.setValue(key, forKey: "msgID")
        dbConversationRef.child("last_message").setValue(conversationDict)
        
        DispatchQueue.main.async {
            self.txtView.text = ""
            self.reloadTableAndScrollToBottom()
        }
        //        self.tableReloadTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.reloadTableAndScrollToBottom), userInfo: nil, repeats: false)
        isTyping = false
        changeStatusinDB(isTyping: false)
    }
    
    //Fetched attributed text to display in textview in cell
    func getConfiguredAttributedString(text : String) -> NSAttributedString
    {
        let myAttribute = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12.0), NSAttributedStringKey.foregroundColor:UIColor(red: 51.0/255.0, green: 51/255.0, blue: 51/255.0, alpha: 1.0)]
        let attributedString = NSAttributedString(string: text, attributes: myAttribute)
        return attributedString
    }
    
    func getConfiguredAttributedStringName(text : String) -> NSAttributedString
    {
        let myAttribute = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15, weight: .semibold), NSAttributedStringKey.foregroundColor:UIColor(red: 51.0/255.0, green: 51/255.0, blue: 51/255.0, alpha: 1.0)]
        let attributedString = NSAttributedString(string: text, attributes: myAttribute)
        return attributedString
    }
    
    
    //configure cell width and height according to text size.
    func configureCellSize(cell : ChatUserCell)
    {
        if txtTemp == nil{
            txtTemp = UITextView()
        }
        
        txtTemp?.attributedText = cell.txtviewMessage.attributedText
        let tSize = txtTemp?.sizeThatFits(CGSize(width: maxChatBGViewWidth, height: CGFloat.greatestFiniteMagnitude))
        
        cell.txtviewMessage.frame.size.width = tSize!.width
            < minChatBGViewWidth ? minChatBGViewWidth : tSize!.width
        cell.txtviewMessage.frame.size.height = tSize!.height
        cell.txtviewMessage.translatesAutoresizingMaskIntoConstraints = true
        cell.contentView.reloadInputViews()
    }
    
    
    
    func setAWSCredentials()
    {
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.APSouth1,
                                                                identityPoolId:"ap-south-1:341684dd-c9fe-4459-ab31-93b353c6dc09")
        
        let configuration = AWSServiceConfiguration(region:.APSouth1, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
    }
    
    func deleteFile(_ filePath:URL) {
        guard FileManager.default.fileExists(atPath: filePath.path) else{
            return
        }
        do {
            try FileManager.default.removeItem(atPath: filePath.path)
        }catch{
            fatalError("Unable to delete file: \(error) : \(#function).")
        }
    }
    
    //MARK: - Actions -
    @IBAction func btnBackTapped(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnClearChatTapped(_ sender: Any)
    {
        let alert = UIAlertController(title: "Are you sure you want to clear chat?", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            self.clearChat()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnAttachmentTapped(_ sender: Any)
    {
        self.view.endEditing(true)
        // self.present(self.imagePickerAlert!, animated: true, completion: nil)
        let alert = UIAlertController(title: "Choose Image", message: "", preferredStyle: .actionSheet)
        
        if videoSharing{
            self.imgPicker.mediaTypes = [kUTTypeImage as String,kUTTypeMovie as String]
        }else{
            self.imgPicker.mediaTypes = [kUTTypeImage as String]
        }
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.imgPicker.sourceType = UIImagePickerController.isSourceTypeAvailable(.camera) ? .camera : .photoLibrary
            self.present(self.imgPicker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Library", style: .default, handler: { (action) in
            self.imgPicker.sourceType = .photoLibrary
            self.present(self.imgPicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnSendMessageTapped(_ sender: Any)
    {
        if txtView.text!.trim() != textViewPlaceholder && txtView.text!.trim() != ""
        {
            addMessageToDB(isMedia: false,messageID: "")
        }
    }
}

extension ChatDetailViewController : PlayerProtocol{
    func playURL(url: URL) {
        let videoPlayer = AVPlayer(url: url)
        
        let playerController = AVPlayerViewController()
        playerController.player = videoPlayer
        present(playerController, animated: true)
        {
            videoPlayer.play()
        }
    }
    
    func downloadURL(nameOfFile: String,messageID : String, completion: @escaping (Bool) -> Void)
    {
        //let key = self.messagesRef!.childByAutoId().key
        
        let reference = self.messagesRef.child(self.conversationObj.conversationID).child(messageID)
        
        AWSUtilityManager.shared.downloadFileWith(nameOfFile, messageID: messageID, messageRef: reference)
    }
}

extension ChatDetailViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let img = info["UIImagePickerControllerEditedImage"]
        {
            let imageToUpload : UIImage = img as! UIImage
            self.mediaStr = MessageTypes.PHOTO.rawValue
            self.uploadImageToFirebase(img: imageToUpload)
        }
        else{
            if let img = info["UIImagePickerControllerOriginalImage"]{
                let imageToUpload : UIImage = img as! UIImage
                self.mediaStr = MessageTypes.PHOTO.rawValue
                self.uploadImageToFirebase(img: imageToUpload)
            }
        }
        
        if let mediaType = info["UIImagePickerControllerMediaType"]
        {
            if mediaType as! String == "public.movie"
            {
                if let videoURL = info[UIImagePickerControllerMediaURL]
                {
                    let uploadFileURL = videoURL as! URL
                    self.mediaStr = MessageTypes.VIDEO.rawValue
                    
                    let fileName = "\(Int64(Date().timeIntervalSince1970 * 1000).description).mp4"
                    
                    self.encodeVideo(videoURL: uploadFileURL,fileName:fileName)
                }
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension ChatDetailViewController : UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView)
    {
        if textView.text.characters.count > 0
        {
            if !isTyping
            {
                isTyping = true
                changeStatusinDB(isTyping: true)
            }
            self.btnSendMessage.isEnabled = true
            
        }else
        {
            if isTyping
            {
                isTyping = false
                changeStatusinDB(isTyping: false)
            }
            self.btnSendMessage.isEnabled = false
            
        }
        
        if (textView.contentSize.height < (SCREEN_SIZE.height * 0.25))
        {
            UIView.animate(withDuration: 0.5, animations:
                {
                    
                    self.txtViewHeight.constant = textView.contentSize.height
            })
        }
        
        if textView.contentSize.height < 40
        {
            self.txtViewHeight.constant = 40
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text.trim() == textViewPlaceholder{
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text.trim() == ""
        {
            textView.text = textViewPlaceholder
        }
        changeStatusinDB(isTyping: false)
    }
}
extension ChatDetailViewController : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if txtTemp == nil{
            txtTemp = UITextView()
        }
        
        
        //let data = messages[indexPath.row]
        let tdata = self.sectionData[self.sortedKeys[indexPath.section]]
        let data = tdata![indexPath.row]
        
        if data.messageType == MessageTypes.PHOTO.rawValue || data.messageType == MessageTypes.VIDEO.rawValue{
            return 250
        }
        
        //txtTemp.text = data.message
        txtTemp?.attributedText = getConfiguredAttributedString(text: data.messageText)
        let tSize = txtTemp?.sizeThatFits(CGSize(width: maxChatBGViewWidth, height: CGFloat.greatestFiniteMagnitude))
        
        if data.user != kcurrentUSERID && conversationObj.isGroup{
            return tSize!.height + 70
        }else{
            return tSize!.height + 50
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! ProfileHeaderCell
        
        // let t = Array(self.sectionData.keys)
        //print(t[section])
        cell.lblTitle.text = self.sortedKeys[section]
        cell.lblTitle.cornerRadius = 5
        cell.lblTitle.clipsToBounds = true
        return cell.contentView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.sectionData.keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        //let t = Array(self.sectionData.keys)
        if let msgs = self.sectionData[self.sortedKeys[section]]
        {
            return msgs.count
        }
        
        return 0
        
        //        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // let t = Array(self.sectionData.keys)
        let tdata = self.sectionData[self.sortedKeys[indexPath.section]]
        let data = tdata![indexPath.row]
        //   let data = messages[indexPath.row]
        
        if data.user == kcurrentUSERID
        {
            if data.messageType == MessageTypes.PHOTO.rawValue || data.messageType == MessageTypes.VIDEO.rawValue
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderMediaCell") as! MediaCell
                
                if !show_message_sent_indicator
                {
                    if cell.imgTick != nil{
                        cell.imgTick.isHidden = true
                    }
                    
                }
                cell.spinnerView.isHidden = true
                cell.btnRetryAction.isHidden = true
                cell.btnVideoAction.isHidden = true
                cell.progressView.isHidden = true
                
                cell.messageID = data.id
                cell.conversationID = self.conversationObj.conversationID
                cell.messageRef = self.messagesRef
                cell.playerDelegate = self
                cell.isDownloadCell = false
                
                cell.accessibilityIdentifier = "SenderMediaCell"
                displayDataInMediaCell(cell: cell, data: data)
                
                
                return cell
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderCell") as! ChatUserCell
                
                if !show_message_sent_indicator
                {
                    if cell.imgTick != nil{
                        cell.imgTick.isHidden = true
                    }
                }
                
                cell.accessibilityIdentifier = "SenderCell"
                displayDataInTextCell(cell:cell,data: data)
               // cell.txtviewMessage.textColor = UIColor.white
                configureCellSize(cell: cell)
                return cell
            }
            
        }
        else
        {
            messagesRef.child(self.conversationObj.conversationID).child(data.id).child("message_status").child(kcurrentUSERID).setValue("r")
            
            if data.messageType == MessageTypes.PHOTO.rawValue || data.messageType == MessageTypes.VIDEO.rawValue
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverMediaCell") as! MediaCell
                cell.spinnerView.isHidden = true
                cell.btnRetryAction.isHidden = true
                cell.btnVideoAction.isHidden = true
                cell.progressView.isHidden = true
                
                cell.messageID = data.id
                cell.conversationID = self.conversationObj.conversationID
                cell.messageRef = self.messagesRef
                cell.playerDelegate = self
                cell.isDownloadCell = true
                cell.accessibilityIdentifier = "ReceiverMediaCell"
                displayDataInMediaCell(cell: cell, data: data)
                return cell
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverCell") as! ChatUserCell
                cell.accessibilityIdentifier = "ReceiverCell"
                displayDataInTextCell(cell:cell,data: data)
                cell.txtviewMessage.textColor = UIColor.black
                configureCellSize(cell: cell)
                return cell
            }
            
        }
    }
}
