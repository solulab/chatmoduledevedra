//
//  BaseViewController.swift
//  FirebaseExtension
//
//  Created by User on 3/5/18.
//  Copyright © 2018 User. All rights reserved.
//

import UIKit
import Firebase

class BaseViewController: UIViewController
{
    var chatUsersList = [ChatUser]()
    var groupList = [ChatGroup]()
    
    var delegate : ChatUserDetailsProtocol?
    var conversationRef : DatabaseReference!
    var userTableRef : DatabaseReference!
    var groupTableRef : DatabaseReference!
    var messagesRef : DatabaseReference!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        self.conversationRef = Database.database().reference().child("conversations")
        self.userTableRef = Database.database().reference().child("users")
        self.groupTableRef = Database.database().reference().child("groups")
        
       
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
extension BaseViewController : ChatUserDetailsProtocol
{
    func getGroupNameForConvID(_ convID: String?, completion: @escaping (ChatGroup) -> Void)
    {
        if let id = convID
        {
            //this will check if user already exist in our local array i.e chatUsersList
            let arr = groupList.filter({ (obj) -> Bool in
                if obj.conversationID == convID!
                {
                    completion(obj)
                    return true
                }else
                {
                    return false
                }
            })
            
            if arr.count > 0
            {
                return
            }
            
            self.groupTableRef.child(id).observe(DataEventType.value, with: { (snapshot) in
                if snapshot.childrenCount > 0
                {
                    //getting all conversations 1 by 1
                    //iterating through all the values
                    let dict = snapshot.value as! Dictionary<String,Any>
                    let obj = ChatGroup()
                    obj.update(data: dict)
                    
                    if let index = self.groupList.index(where: {$0.user_id == obj.user_id}) {
                        self.groupList.remove(at: index)
                        self.groupList.insert(obj, at: index)
                    }else{
                        self.groupList.append(obj)
                    }
                    completion(obj)
                }
            })
        }
    }
    
    func getUserNameForUserID(_ userID: String?, completion: @escaping(_ userObj: ChatUser) -> Void)
    {
        
        if let id = userID
        {
            //this will check if user already exist in our local array i.e chatUsersList
            let arr = chatUsersList.filter({ (obj) -> Bool in
                if obj.user_id == userID!
                {
                    completion(obj)
                    return true
                }else
                {
                    return false
                }
            })
            
            if arr.count > 0
            {
                return
            }
            
            self.userTableRef.child(id).observe(DataEventType.value, with: { (snapshot) in
                
                if snapshot.childrenCount > 0
                {
                    //getting all conversations 1 by 1
                    //iterating through all the values
                    let dict = snapshot.value as! Dictionary<String,Any>
                    let obj = ChatUser()
                    obj.update(data: dict)
                    
                    if let index = self.chatUsersList.index(where: {$0.user_id == obj.user_id}) {
                        self.chatUsersList.remove(at: index)
                        self.chatUsersList.insert(obj, at: index)
                    }else{
                        self.chatUsersList.append(obj)
                    }
                    completion(obj)
                }
            })
        }
        
    }
}
