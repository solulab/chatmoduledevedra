//
//  UserListViewController.swift
//  FirebaseExtension
//
//  Created by User on 3/1/18.
//  Copyright © 2018 User. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage
import CoreData

//user_4
//rohan
//user_2
//kunal
//let kcurrentUSERID = "user_2"
let kcurrentUSERID = "user_4"

let kcurrenStaticName = "Dev"

class UserListViewController: BaseViewController
{
    // Firebase DB/table reference
    var list = [Conversation]()//Custom class instance
    var offlineList = [ConversationEntity]()
    @IBOutlet var tblView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeMessageRef()
        //reference of conversation table
      //  checkUserData()
        fetchConversations()
       // self.getData()
      //  fetchData(checkForMessage: false)
        addValueChangedObserver()
//        if !AppInstance!.isInternetAvailable()
//        {
//        }else{
//
//        }
   }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    override func viewDidAppear(_ animated: Bool) {
      //  createConversation()
        
       // createGroup()
//        for i in 1 ..< 10{
//            createUsers(i: i)
//        }
    }
    
    func initializeMessageRef()
    {
       // Database.database().isPersistenceEnabled = true

        //if messagesRef == nil{
            messagesRef = Database.database().reference().child("messages")
       // }
        messagesRef.keepSynced(true)
    }
    
    //MARK: - Functions -
    
    //Getting list of users
    func fetchConversations()
    {
        self.conversationRef.queryOrdered(byChild: "users/\(kcurrentUSERID)").queryEqual(toValue: "true").observe(.childAdded, with: { (snapshot) in
            
            
            //if the reference have some values
            if snapshot.childrenCount > 0
            {
                //getting all conversations 1 by 1
                //iterating through all the values
                    let dict = snapshot.value as! Dictionary<String,Any>
                    let obj = Conversation()
                    obj.update(data: dict)
                    obj.conversationID = snapshot.key
               
//                if(self.isExist(convID: obj.conversationID, convObj: obj)){
//                    print("exist")
//                }else{
//                    self.addDataInLocalDB(dictObj: dict, conversationID: obj.conversationID)
//                    print("exist")
//
//                }
                if showGroups == false{
                    if !obj.isGroup{
                        self.list.append(obj)
                    }
                }else{


                    self.list.append(obj)
                }
                
            }
            self.sortArray()
            self.tblView.reloadData()
        })
    }
    var convEntity = ConversationEntity()
    //check data is there in local db or not
    func isExist(convID : String,convObj : Conversation) -> Bool{
        let managedContext = AppInstance.persistenContainer.viewContext
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ConversationEntity")
            fetchRequest.predicate = NSPredicate(format: "conversationID == %@", convID)
            let data = try managedContext.fetch(fetchRequest)
            for dataOBj in data {
                if let con = dataOBj as? ConversationEntity {
                    print(con)
                    con.datetime = convObj.lastMessage.messageData.timestamp
                    con.message = convObj.lastMessage.messageData.messageText
                    con.convData = convObj.toDatabaseDict(data: convObj)
                    do {
                        try managedContext.save()
                        
                    } catch {
                        print("Failed saving")
                    }
                    return true
                }
            }
            return false
        }
        catch
        {
            print("Fetching Failed")
        }
        return false
    }
    

    func isExistUserData(convObj : ChatUser) -> Bool{
        let managedContext = AppInstance.persistenContainer.viewContext
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserList")
            let data = try managedContext.fetch(fetchRequest)
            for dataOBj in data {
                if let con = dataOBj as? ConversationEntity {
                   
                    
                    return true
                }
            }
            return false
        }
        catch
        {
            print("Fetching Failed")
        }
        return false
    }

    //fetches data from local db
    
    func fetchData(checkForMessage:Bool)
    {
        let managedContext = AppInstance.persistenContainer.viewContext
        
        do
        {
            let data = try managedContext.fetch(NSFetchRequest(entityName: "ConversationEntity"))
//            offlineList.removeAll()
//            list.removeAll()
            for i in data as! [ConversationEntity]
            {
                print(i)
                self.offlineList.append(i)
                let objConv = Conversation()
                let convDict = objConv.toConvDict(data: i)
                self.list.append(convDict)
            }
            self.tblView.reloadData()
            if(checkForMessage){
                
            }else{
                DispatchQueue.global(qos: .background).async {
                    self.fetchConversations()
                }

            }

            
        }
        catch
        {
            print("Fetching Failed")
        }
    }
    
    func getData(){
        let managedContext = AppInstance.persistenContainer.viewContext

        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ConversationEntity")
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        do {
            let result = try managedContext.fetch(request)
            for data in result as! [NSManagedObject] {
                        print(data.value(forKey: "conversationID") as! String)
            
            }
            
        } catch {
            
            print("Failed")
        }

    }
    //insert data in local db
    func addDataInLocalDB(dictObj : Dictionary<String,Any>,conversationID : String)
    {
        let managedContext = AppInstance.persistenContainer.viewContext
        let entity =
            NSEntityDescription.entity(forEntityName: "ConversationEntity",
                                       in: managedContext)!

        let dbObj = NSManagedObject(entity: entity,
                                     insertInto: managedContext)
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ConversationEntity")
        request.returnsObjectsAsFaults = false
        do {
            let result = try managedContext.fetch(request)
            print(result)
                dbObj.setValue(conversationID, forKey: "conversationID")
                dbObj.setValue(dictObj, forKey: "convData")
//            con.datetime = convObj.lastMessage.timestamp
//                con.message = convObj.lastMessage.messageText

        } catch {
            print("Failed")
        }
        do {
            try managedContext.save()
           // self.fetchData(checkForMessage: true)
        } catch {
            print("Failed saving")
        }

    }
    func addUserDataInLocalDB(convObj : ChatUser)
    {
        let managedContext = AppInstance.persistenContainer.viewContext
        let entity =
            NSEntityDescription.entity(forEntityName: "UserList",
                                       in: managedContext)!
        
        let dbObj = NSManagedObject(entity: entity,
                                    insertInto: managedContext)
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "UserList")
        request.returnsObjectsAsFaults = false
        do {
            let result = try managedContext.fetch(request)
            print(result)
        } catch {
            
            print("Failed")
        }
        do {
            try managedContext.save()
        } catch {
            print("Failed saving")
        }
        
    }
    
    func checkUserData(){
        self.userTableRef.observe(DataEventType.value, with: { (snapshot) in
            if snapshot.childrenCount > 0
            {
                let dict = snapshot.value as! Dictionary<String,Any>
                let obj = ChatUser()
                obj.update(data: dict)
                if(self.isExistUserData(convObj: obj)){
                    
                    print("exist")
                }else{
                    self.addUserDataInLocalDB(convObj: obj)
                    print("exist")
                    
                }

//                if let index = self.chatUsersList.index(where: {$0.user_id == obj.user_id}) {
//                    self.chatUsersList.remove(at: index)
//                    self.chatUsersList.insert(obj, at: index)
//                }else{
//                    self.chatUsersList.append(obj)
//                }
                //completion(obj)
            }
        })
    }
    
    func addValueChangedObserver()
    {
        conversationRef!.observe(.childChanged, with: { (snapshot) in

            if snapshot.childrenCount > 0
            {
                let dict = snapshot.value as! Dictionary<String,Any>
                let obj = Conversation()
                obj.update(data: dict)
                obj.conversationID = snapshot.key

                if let index = self.list.index(where: {$0.conversationID == obj.conversationID})
                {
                    let tObj = self.list[index]
                    self.list.remove(at: index)
                    obj.isValueChanged = true
                  
                    self.list.insert(obj, at: index)
                    
                    if tObj.lastMessage.messageId == obj.lastMessage.messageId
                    {
                        self.tblView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
                    }
                    else
                    {
                        self.sortArray()
                        self.tblView.reloadData()
                    }
                }
           }
        })
    }
    
    func sortArray()
    {
        self.list.sort(by:{ (obj2, obj1) -> Bool in
            if obj2.lastMessage.messageData.timestamp.characters.count > 0 && obj1.lastMessage.messageData.timestamp.characters.count > 0
            {
                if Int64(obj2.lastMessage.messageData.timestamp)! > Int64(obj1.lastMessage.messageData.timestamp)!
                {
                    return true
                }
            }
            return false
        })
    }
    
    func getUsers() -> NSMutableDictionary
    {
        let dict = NSMutableDictionary()
        dict.setValue("true", forKey: "user_2")
        dict.setValue("true", forKey: "user_4")
        return dict
    }
    
    func createGroup()
    {
        let dict = NSMutableDictionary()
        dict.setValue("-L6WI6fXIpSZgSMde7Ov", forKey: "groupID")
        dict.setValue("testGroup2", forKey: "name")
        dict.setValue("", forKey: "image")
        let userRef = Database.database().reference().child("groups")
        userRef.child("-L6WI6fXIpSZgSMde7Ov").setValue(dict)
    }
    
    func createUsers(i : Int)
    {
        let dict = NSMutableDictionary()
        dict.setValue("user_\(i)", forKey: "userID")
        dict.setValue("aabid", forKey: "name")
        dict.setValue("", forKey: "image")
        let userRef = Database.database().reference().child("users")
        userRef.child("user_\(i)").setValue(dict)
    }
    
    //Insert data in firebase table "Conversation"
    func createConversation()
    {
        let conversationDict = NSMutableDictionary()
        
        let key = "user2_user4"
        
        conversationDict.setValue(getUsers(), forKey: "Typing")
        conversationDict.setValue(getUsers(), forKey: "chatHistory")
        conversationDict.setValue("false", forKey: "isGroup")
        
        let dict = NSMutableDictionary()
        dict.setValue(key, forKey: "conversationID")
        dict.setValue("abc", forKey: "text")
        dict.setValue(Int64(Date().timeIntervalSince1970 * 1000).description, forKey: "timestamp")
        dict.setValue("text", forKey: "type")
        conversationDict.setValue(dict, forKey: "last_message")
        conversationDict.setValue(getUsers(), forKey: "users")
        conversationRef.child(key).setValue(conversationDict)
    }
}
extension UserListViewController : MessageCountProtocol
{
    func getUnreadMessagesCount(obj : Conversation,completion: @escaping(_ counter : Int) -> Void)
    {
        self.messagesRef.child(obj.conversationID).removeAllObservers()
        self.messagesRef.child(obj.conversationID).queryOrdered(byChild: "message_status/\(kcurrentUSERID)").queryEqual(toValue: "s").observe(.value, with: { (snapshot) in
            completion(Int(snapshot.childrenCount))
        })
    }
}
extension UserListViewController : UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
      //  if !AppInstance!.isInternetAvailable(){
         //   return offlineList.count
       // }
        return list.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatUserCell") as! ChatUserCell
        
//        if !AppInstance!.isInternetAvailable()
//        {
//            let offlineData = self.list[indexPath.row]
//            cell.userListDelegate = self
//            cell.delegate = self
//            cell.cellData = offlineData
//            cell.displayCellData()
//            cell.lblUnReadMsgCount.layer.cornerRadius = 12.5
//            cell.lblUnReadMsgCount.clipsToBounds = true
         //   let typing =
//        let typingDict = dictMessage("Typing")
        
           // cell.lblMessage.text = dictMessage
           // cell.lblUserName.text = offlineData.username
           // cell.lblTime.text = offlineData.message
//            if offlineData.userimage != nil{
//                cell.imgDP.sd_setImage(with: URL(string: offlineData.userimage!), completed: nil)
//            }
//        }
//        else
//        {
            cell.userListDelegate = self
            cell.delegate = self
           // cell.messageCountDelegate = self

            let data = list[indexPath.row]
            cell.cellData = data
            cell.displayCellData()
            cell.lblUnReadMsgCount.layer.cornerRadius = 12.5
            cell.lblUnReadMsgCount.clipsToBounds = true


            if data.isGroup
            {
                cell.displayGroupDetail(conversationID: data.conversationID)
            }else
            {
                if let dict = data.userDictionary
                {
                    for(key,_) in dict{
                        if (key as! String) != kcurrentUSERID
                        {
                            cell.displayUserDetail(userID: (key as! String))
                        }
                    }
                }
            }

            data.isValueChanged = false
//
//        }
       return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vc = UIStoryboard(name: "Chat", bundle: nil).instantiateViewController(withIdentifier: "ChatDetailViewController") as! ChatDetailViewController
        
       let data = list[indexPath.row]
        self.messagesRef.child(data.conversationID).child("message_status").child(kcurrentUSERID).setValue("r")
        vc.conversationObj = data
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
