//
//  Protocols.swift
//  FirebaseExtension
//
//  Created by User on 3/5/18.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation

protocol ChatUserDetailsProtocol
{
    func getUserNameForUserID(_ userID: String?, completion: @escaping(_ userObj: ChatUser) -> Void)
    
    func getGroupNameForConvID(_ convID: String?, completion: @escaping (ChatGroup) -> Void)
}

protocol PlayerProtocol
{
    func playURL(url : URL)
    func downloadURL(nameOfFile : String,messageID : String, completion: @escaping(_ isDownloaded: Bool) -> Void)
}

protocol MessageCountProtocol
{
    func getUnreadMessagesCount(obj : Conversation,completion: @escaping(_ counter : Int) -> Void)
}
