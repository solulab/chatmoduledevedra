//
//  Constants.swift
//  FirebaseExtension
//
//  Created by User on 3/7/18.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation

let showTypingIndicator = true
let showGroups = true
let videoSharing = true
let show_message_sent_indicator = true



//Constants
let CHAT_DATE_FORMAT = "MMMM dd, yyyy"
let CHAT_LIST_DATE_FORMAT = "dd/MM/yy"
