//
//  Helper.swift
//  FirebaseExtension
//
//  Created by User on 3/5/18.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation
import UIKit

class Helper
{
    class func getFormattedDateTime(secondss : Double,isChatDetailScreen : Bool) -> String
    {
        //secondss is in miliseconds so divided by 1000
        let seconds = secondss / 1000
        
        let isToday = Calendar.current.isDateInToday(Date(timeIntervalSince1970: seconds))
        
        let isYesterday = Calendar.current.isDateInYesterday(Date(timeIntervalSince1970: seconds))
        
        let tDate = Date(timeIntervalSince1970: seconds)
        
        var str = ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        
        if isToday
        {
            if isChatDetailScreen{
                str = "Today"
            }
            else{
                str = dateFormatter.string(from: tDate)
            }
        }else if isYesterday
        {
            str = "Yesterday"
        }else
        {
            var dateFormat = ""
            if isChatDetailScreen{
                dateFormat = CHAT_DATE_FORMAT
            }else{
                dateFormat = CHAT_LIST_DATE_FORMAT
            }
            dateFormatter.dateFormat = dateFormat
            str = "\(dateFormatter.string(from: tDate))"
        }
        return str
    }
    
   class func getFilePath(nameOfFile : String) -> URL
    {
        let filePath = self.getDocumentDirectoryPath().appendingPathComponent(nameOfFile)
        return filePath
    }
    
    class func getDocumentDirectoryPath() -> URL
    {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        return URL(fileURLWithPath: path)
    }
    
    class func isFileExistInDevice(nameOfFile : String) -> Bool
    {
        let filePath = getDocumentDirectoryPath().appendingPathComponent(nameOfFile).path
        let fileManager = FileManager.default
        
        if fileManager.fileExists(atPath: filePath) {
            return true
        } else {
            return false
        }
    }
    
    class func displayAlert(title : String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        AppInstance.window!.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
}
