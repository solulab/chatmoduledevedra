//
//  Conversations.swift
//  FirebaseExtension
//
//  Created by User on 3/1/18.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation

enum MessageType : String
{
    case TextMessage = "text"
    case MediaMessagePhoto = "mediaPhoto"
    case MediaMessageVideo = "mediaVideo"
}

class Conversation
{
    var conversationID = ""
    var typingDictionary : NSMutableDictionary?
    var chatHistoryDictionary : NSMutableDictionary?
    var isGroup = false
    var lastMessage = LastMessage()
    
    var userDictionary : NSMutableDictionary?
   
    var isValueChanged = false
    var unReadMessageCount = 0
    //for offline support
    var username = ""
    var userImage = ""
  
    init() {
        
    }
    
    func toConvDict(data : ConversationEntity) -> Conversation {
        let dict = Conversation()
        dict.conversationID = data.conversationID ?? ""
        dict.chatHistoryDictionary = data.convData?.value(forKey: "chatHistory") as? NSMutableDictionary
        dict.lastMessage.update(data: data.convData?.value(forKey: "last_message")! as! NSDictionary)
        dict.userDictionary = data.convData?.value(forKey: "users") as? NSMutableDictionary //("chatHistory")
        dict.typingDictionary = data.convData?.value(forKey: "Typing") as? NSMutableDictionary
        return dict
        
    }
    func toDatabaseDict(data : Conversation) -> NSMutableDictionary {
        let dict = NSMutableDictionary()
            //dict.conversationID = data.conversationID
        dict.setValue(data.chatHistoryDictionary, forKey: "chatHistory")
       // let lsatmessage = data.lastMessage as? NSMutableDictionary\
        let dictMessage = data.lastMessage.toConvDict(data: data.lastMessage)
        dict.setValue(dictMessage, forKey: "last_message")
        dict.setValue(data.userDictionary, forKey: "users")
        dict.setValue(data.typingDictionary, forKey: "Typing")
        return dict
        
    }

    init(conversationID: String,typingDictionary: NSMutableDictionary, chatHistoryDictionary: NSMutableDictionary,isGroup : Bool,lastMessage : LastMessage, userDictionary:NSMutableDictionary, isValueChanged : Bool,unReadMessageCount:Int) {
        self.conversationID = conversationID
        self.typingDictionary = typingDictionary
        self.chatHistoryDictionary = chatHistoryDictionary
        self.isGroup = isGroup
        self.lastMessage = lastMessage
        self.userDictionary = userDictionary
        self.isValueChanged = isValueChanged
        self.unReadMessageCount = unReadMessageCount
    }
    func update(data : Dictionary<String,Any>)
    {
        if let dict = data["Typing"]
        {
            typingDictionary = NSMutableDictionary(dictionary: dict as! NSDictionary)
        }
        
        if let dict = data["chatHistory"]
        {
            chatHistoryDictionary = NSMutableDictionary(dictionary: dict as! NSDictionary)
        }
        
        if let dict = data["users"]
        {
            userDictionary = NSMutableDictionary(dictionary: dict as! NSDictionary)
        }
        
        if let dict = data["last_message"]
        {
            lastMessage.update(data: dict as! NSDictionary)
            //lastMessageDictionary = NSMutableDictionary(dictionary: dict as! NSDictionary)
        }
        
        if let data = data["isGroup"]{
            if (data as! String) == "true"{
                isGroup = true
            }
        }
        
    }
}

class LastMessage
{
    var messageId = ""
    var messageData = Message()
    
    func toConvDict(data : LastMessage) -> NSMutableDictionary {
        let dict = NSMutableDictionary()
        dict.setValue(data.messageId, forKey: "messageId")
        let dictMessageData = messageData.toConvDict(data: messageData)
        dict.setValue(dictMessageData, forKey: "messageData")
        return dict
        
    }

    func update(data : NSDictionary)
    {
        if data.value(forKey: "text") != nil{
            messageData.messageText = data.value(forKey: "text") as! String
        }
        
        if data.value(forKey: "type") != nil
        {
            if data.value(forKey: "type") as! String == "photo"{
                messageData.messageType = MessageType.MediaMessagePhoto.rawValue
            }else if data.value(forKey: "type") as! String == "video"{
                messageData.messageType = MessageType.MediaMessageVideo.rawValue
            }
        }
        
        if let data = data["timestamp"]{
            messageData.timestamp = data as! String
        }
        
        if let data = data["msgID"]{
            messageId = data as! String
        }
    }
}

class Message
{
    var id = ""
    var messageText = ""
    var timestamp = ""
    var status = ""
    var messageType = "text"
    var senderName = ""
    var user = ""
    var mediaURL = ""
    var videoThumbnail = ""
    
    func toConvDict(data : Message) -> NSMutableDictionary {
        let dict = NSMutableDictionary()
        dict.setValue(data.id, forKey: "id")
        dict.setValue(data.messageText, forKey: "messageText")
        dict.setValue(data.timestamp, forKey: "timestamp")
        dict.setValue(data.status, forKey: "status")
        dict.setValue(data.messageType, forKey: "messageType")
        dict.setValue(data.senderName, forKey: "senderName")
        dict.setValue(data.user, forKey: "user")
        dict.setValue(data.mediaURL, forKey: "mediaURL")
        dict.setValue(data.videoThumbnail, forKey: "videoThumbnail")
        return dict
        
    }

    func update(data : Dictionary<String,Any>)
    {
        if let data = data["senderName"]
        {
            senderName = data as! String
        }
        
        if let data = data["time"]
        {
            timestamp = data as! String
        }
        
        if let data = data["message"]
        {
            messageText = data as! String
        }
        
        if let data = data["user"]
        {
            user = data as! String
        }
        
        if let data = data["mediaURL"]
        {
            mediaURL = data as! String
        }
        
        if let str = data["videoThumb"]
        {
            videoThumbnail = str as! String
        }
        
        if let data = data["type"]
        {
            if (data as! String) == MessageTypes.PHOTO.rawValue{
                messageType = MessageTypes.PHOTO.rawValue
            }else if (data as! String) == MessageTypes.VIDEO.rawValue{
                messageType = MessageTypes.VIDEO.rawValue
            }else{
                messageType = data as! String
            }
        }
    }
}

class ChatUser
{
    var user_id = ""
    var name = ""
    var imageURL = ""
    
    func update(data : Dictionary<String,Any>)
    {
        if let data = data["userID"]
        {
            user_id = data as! String
        }
        
        if let data = data["image"]
        {
            imageURL = data as! String
        }
        
        if let data = data["name"]
        {
            name = data as! String
        }
    }
}

class ChatGroup : ChatUser{
    var conversationID = ""
    
    override func update(data : Dictionary<String,Any>)
    {
        super.update(data: data)
        
        if let data = data["groupID"]
        {
            conversationID = data as! String
        }
    }
}

class Sections
{
    var sectionTitle = ""
    var sectionMessages = [Message]()
}
