//
//  AWSUtilityManager.swift
//  HelloLayover
//
//  Created by user on 11/16/17.
//  Copyright © 2017 SoluLab. All rights reserved.
//

import Foundation
import AWSS3
import FirebaseDatabase
import Photos

enum RequestState {
    case paused
    case running
    case notFound
    case canceled
}


let AWSNotificationProgress = NSNotification.Name(rawValue: "AWSNotificationProgress")
let AWSNotificationFailed = NSNotification.Name(rawValue: "AWSNotificationFailed")

class AWSUtilityManager
{
    static var shared = AWSUtilityManager()
    
    init() {
        
        if let list = UserDefaults.standard.value(forKey: "UploadRequests") as? [String: [String: String]]
        {
            self.uploadingList = list
        }
    }
    
    var uploadRequestArray = [MediaUploadingRequest]()
    var downloadRequestArray = [MediaUploadingRequest]()
    
    var uploadingList = [String: [String: String]]()
    {
        didSet{
            
            UserDefaults.standard.set(uploadingList, forKey: "UploadRequests")
            //            UserDefaults.standard.set(uploadingList, forKey: "OLDRequests")
        }
    }
    
    func getCurrentProgressFor(_ messageID: String,isDownload : Bool) -> CGFloat
    {
        if isDownload{
            if let first = downloadRequestArray.first(where: { (mediaRequest) -> Bool in
                
                return mediaRequest.messageId == messageID
            })
            {
                return first.progressValue
                
            }
        }else{
            if let first = uploadRequestArray.first(where: { (mediaRequest) -> Bool in
                
                return mediaRequest.messageId == messageID
            })
            {
                return first.progressValue
                
        }
        
        }
        return 0.0
    }
    
    func getURLDetailsFor(_ messageID: String) -> [String : String]?
    {
        return self.uploadingList[messageID]
    }
    
    //    #MARK: - Uploading
    
    func uploadFileWith(_ fileName: String, fileURL: URL, key: String, messageRef: DatabaseReference) -> Bool
    {
        if FileManager.default.fileExists(atPath: fileURL.relativePath) == false
        {
            return false
        }
        
        //        check if request already exist
        if let first = uploadRequestArray.first(where: { (mediaRequest) -> Bool in
            
            return mediaRequest.messageId == key
        })
        {
            //            if request already exist, then check state
            if first.uploadTransferRequest?.state == AWSS3TransferManagerRequestState.canceling
            {
                //                if request state is cancel, then remove it from list and create new request
                
                if let index = self.uploadRequestArray.index(where: {$0.messageId == first.messageId }) {
                    self.uploadRequestArray.remove(at: index)
                }
                self.uploadingList.removeValue(forKey: key)
                
                let request = MediaUploadingRequest(fileName: fileName, uploadingFileURL: fileURL, messageId: key, messageRef: messageRef)
                request.startUploadingFile()
                self.uploadRequestArray.append(request)
                
                self.uploadingList[key] = ["FileURL": fileURL.relativePath, "FileName": fileName]
            }
            else
            {
                //                else start that request again
                first.startUploadingFile()
            }
        }
        else
        {
            let request = MediaUploadingRequest(fileName: fileName, uploadingFileURL: fileURL, messageId: key, messageRef: messageRef)
            request.startUploadingFile()
            self.uploadRequestArray.append(request)
            
            self.uploadingList[key] = ["FileURL": fileURL.relativePath, "FileName": fileName]
        }
        return true
    }
    
    func downloadFileWith(_ fileName: String, messageID: String, messageRef: DatabaseReference)
    {
        //        check if request already exist
        if let first = downloadRequestArray.first(where: { (mediaRequest) -> Bool in
            
            return mediaRequest.messageId == messageID
        })
        {
            //            if request already exist, then check state
            if first.uploadTransferRequest?.state == AWSS3TransferManagerRequestState.canceling
            {
                //                if request state is cancel, then remove it from list and create new request
                if let index = self.downloadRequestArray.index(where: {$0.messageId == first.messageId }) {
                    self.downloadRequestArray.remove(at: index)
                }
                self.uploadingList.removeValue(forKey: messageID)
                
                
                let request = MediaUploadingRequest(fileName: fileName, messageId: messageID, messageRef: messageRef)
                request.startDownloadingFile()
                self.downloadRequestArray.append(request)
                self.uploadingList[messageID] = ["FileURL": "", "FileName": fileName]
            }
            else
            {
                //                else start that request again
                first.startDownloadingFile()
            }
        }
        else
        {
            let request = MediaUploadingRequest(fileName: fileName, messageId: messageID, messageRef: messageRef)
            request.startDownloadingFile()
            self.downloadRequestArray.append(request)
            self.uploadingList[messageID] = ["FileURL": "", "FileName": fileName]
            
        }
        
    }
    
    
    func currentStatOfRequest(_ messageID: String, isForDownload: Bool) -> RequestState {
        
        if isForDownload == true
        {
            if let first = downloadRequestArray.first(where: { (mediaRequest) -> Bool in
                
                return mediaRequest.messageId == messageID
            })
            {
                if first.downloadTranferRequest?.state == AWSS3TransferManagerRequestState.running
                {
                    return RequestState.running
                }
                else if first.downloadTranferRequest?.state == AWSS3TransferManagerRequestState.paused
                {
                    return RequestState.paused
                }
                else
                {
                    return RequestState.canceled
                }
            }
            else
            {
                return .notFound
            }
        }
        else
        {
            if let first = uploadRequestArray.first(where: { (mediaRequest) -> Bool in
                
                return mediaRequest.messageId == messageID
            })
            {
                if first.uploadTransferRequest?.state == AWSS3TransferManagerRequestState.running
                {
                    return RequestState.running
                }
                else if first.uploadTransferRequest?.state == AWSS3TransferManagerRequestState.paused
                {
                    return RequestState.paused
                }
                else
                {
                    let fileURL = first.uploadingFileURL
                    
                    if FileManager.default.fileExists(atPath: fileURL!.relativePath) == false
                    {
                        return RequestState.notFound
                    }
                    else
                    {
                        return RequestState.canceled
                    }
                }
            }
            else
            {
                if let _ = self.getURLDetailsFor(messageID)
                {
                    return .canceled
                }
                else
                {
                    return .notFound
                }
            }
        }
        
    }
    
    
    
    func pauseUploadWith(_ messageID: String, isForDownload: Bool)
    {
        if isForDownload == false
        {
            if let first = uploadRequestArray.first(where: { (mediaRequest) -> Bool in
                
                return mediaRequest.messageId == messageID
            })
            {
                _ = first.uploadTransferRequest?.pause()
            }
        }
        else
        {
            if let first = downloadRequestArray.first(where: { (mediaRequest) -> Bool in
                
                return mediaRequest.messageId == messageID
            })
            {
                _ = first.downloadTranferRequest?.pause()
            }
        }
    }
}



class MediaUploadingRequest
{
    var fileName : String!
    var uploadingFileURL : URL!
    var messageId : String!
    var messageRef : DatabaseReference!
    var progressValue : CGFloat = 0.0
    var uploadTransferRequest: AWSS3TransferManagerUploadRequest?
    var downloadTranferRequest: AWSS3TransferManagerDownloadRequest?
    
    
    init(fileName: String, uploadingFileURL: URL, messageId: String, messageRef: DatabaseReference) {
        
        self.fileName = fileName
        self.uploadingFileURL = uploadingFileURL
        self.messageId = messageId
        self.messageRef = messageRef
        self.uploadTransferRequest = getUploadRequest()
    }
    
    init(fileName: String, messageId: String, messageRef: DatabaseReference) {
        
        self.fileName = fileName
        self.messageId = messageId
        self.messageRef = messageRef
        self.uploadingFileURL = Helper.getFilePath(nameOfFile: fileName)
        self.downloadTranferRequest = getDownloadRequest()
    }
    
    
    func getUploadRequest() -> AWSS3TransferManagerUploadRequest
    {
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.bucket = AWS_BUCKET_NAME
        uploadRequest?.key = fileName
        uploadRequest?.body = uploadingFileURL!
        uploadRequest?.contentType = "video/mp4"
        return uploadRequest!
    }
    
    func getDownloadRequest() -> AWSS3TransferManagerDownloadRequest
    {
        let downloadRequest = AWSS3TransferManagerDownloadRequest()
        downloadRequest?.bucket = AWS_BUCKET_NAME
        downloadRequest?.key = fileName
        downloadRequest?.downloadingFileURL = uploadingFileURL
        return downloadRequest!
    }
    
    func startUploadingFile()
    {
        self.messageRef?.observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            let thumbnailURL = value?["videoThumb"] as? String ?? ""
            
            if thumbnailURL.characters.count > 0
            {
                self.uploadOriginalFile()
            }
            else
            {
                self.uploadThumbnail()
                self.uploadOriginalFile()
            }
        })
    }
    
    func uploadThumbnail()
    {
        let transferManager = AWSS3TransferManager.default()
        let avAsset = AVURLAsset(url: self.uploadingFileURL)
        let imgGenerator = AVAssetImageGenerator(asset: avAsset)
        imgGenerator.maximumSize = CGSize(width: 96, height: 96)
        do {
            
            let cgThumbnail = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let uiImage = UIImage.init(cgImage: cgThumbnail)
            print("\(uiImage.size)")
            
            let docDir2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as NSURL
            
            let filePath = docDir2.appendingPathComponent(self.fileName)
            
            let fullPath = filePath?.deletingPathExtension().appendingPathExtension("png")
            if FileManager.default.fileExists(atPath: (fullPath?.path)!) == true
            {
                try FileManager.default.removeItem(atPath: (fullPath?.path)!)
            }
            
            let data = UIImagePNGRepresentation(uiImage)
            try data?.write(to: fullPath!)
            
            let uploadRequest1 = AWSS3TransferManagerUploadRequest()
            uploadRequest1?.bucket = AWS_BUCKET_NAME
            uploadRequest1?.key = "thumbnail/\(fullPath!.lastPathComponent)"
            uploadRequest1?.body = fullPath!
            uploadRequest1?.contentType = "image/png"
            uploadRequest1?.acl = .bucketOwnerFullControl
            
            transferManager.upload(uploadRequest1!).continueWith(executor: AWSExecutor.mainThread(), block: { (task) -> Any? in
                if task.error == nil
                {
                    self.messageRef?.child("videoThumb").setValue(fullPath!.lastPathComponent)
                }
                else
                {
                    _ = uploadRequest1?.cancel()
                    NotificationCenter.default.post(name: AWSNotificationFailed, object: ["messageId" : self.messageId, "Error" : task.error!.localizedDescription])
                }
                return nil
            })
            
        }
        catch
        {
            _ = self.uploadTransferRequest?.cancel()
            NotificationCenter.default.post(name: AWSNotificationFailed, object: ["messageId" : self.messageId, "Error" : "Uploading Thumbnail Failed"])
        }
    }
    
    func uploadOriginalFile()
    {
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(self.uploadTransferRequest!).continueWith(executor: AWSExecutor.mainThread())
        { (task) -> Any? in
            
            if task.error == nil
            {
                print(self.messageId)
                self.messageRef?.child("mediaURL").setValue(self.fileName)
                NotificationCenter.default.post(name: AWSNotificationProgress, object: ["messageId" : self.messageId, "Progress" : 1.0])
                if let index = AWSUtilityManager.shared.uploadRequestArray.index(where: {$0.messageId == self.messageId}) {
                    AWSUtilityManager.shared.uploadRequestArray.remove(at: index)
                }
                AWSUtilityManager.shared.uploadingList.removeValue(forKey: self.messageId)
                
                return nil
            }
            else
            {
                if let error = task.error as NSError? {
                    if error.domain == AWSS3TransferManagerErrorDomain, let code = AWSS3TransferManagerErrorType(rawValue: error.code)
                    {
                        switch code
                        {
                        case .cancelled:
                             NotificationCenter.default.post(name: AWSNotificationFailed, object: ["messageId" : self.messageId, "Error" : task.error!.localizedDescription])
                            break
                            
                        case .paused:
                             NotificationCenter.default.post(name: AWSNotificationFailed, object: ["messageId" : self.messageId, "Error" : "Paused"])
                            break
                        default:
                            _ = self.uploadTransferRequest?.cancel()
                            break
                        }
                        
                    }
                    else
                    {
                        _ = self.uploadTransferRequest?.cancel()
                        NotificationCenter.default.post(name: AWSNotificationFailed, object: ["messageId" : self.messageId, "Error" : task.error!.localizedDescription])
                    }
                }
                else
                {
                    _ = self.uploadTransferRequest?.cancel()
                    NotificationCenter.default.post(name: AWSNotificationFailed, object: ["messageId" : self.messageId, "Error" : task.error!.localizedDescription])
                }
                
                
                
                
                print("Error: \(String(describing: task.error))")
                
            }
            return nil
        }
        
        self.uploadTransferRequest!.uploadProgress = {(bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) -> Void in
            DispatchQueue.main.async(execute:
                {() -> Void in
                    let percentUpload : CGFloat = CGFloat((totalBytesSent * 100) / totalBytesExpectedToSend)
                    
//                    print("totalBytesSent : \(totalBytesSent) - totalBytesExpectedToSend: \(totalBytesExpectedToSend)")
                    
                    self.progressValue = CGFloat(percentUpload / 100)
//                    print("Progress Value : \(self.progressValue)")
                    if self.progressValue > 0.99
                    {
                        self.progressValue = 0.99
                    }
                    NotificationCenter.default.post(name: AWSNotificationProgress, object: ["messageId" : self.messageId, "Progress" : self.progressValue])
                    
                    //                    self.delegate?.progressChange(self.messageId, progressValue: self.progressValue)
//                    print("\nTotal Percent upload : \(percentUpload)")
//                    print("\n\nTotal uploads in progress : \(AppInstance!.uploadRequestArray.count)")
//                    print("Message ID ----> :\(self.messageId)")
//                    print("Bytes sent : \(bytesSent)\n\nTotal bytes sent : \(totalBytesSent)\n\nExpected to send \(totalBytesExpectedToSend)")
            })
        }
    }
    
    func startDownloadingFile()
    {
        let transferManager = AWSS3TransferManager.default()
        transferManager.download(self.downloadTranferRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in
            if let error = task.error as NSError? {
                if error.domain == AWSS3TransferManagerErrorDomain, let code = AWSS3TransferManagerErrorType(rawValue: error.code)
                {
                    switch code
                    {
                    case .cancelled:
                         NotificationCenter.default.post(name: AWSNotificationFailed, object: ["messageId" : self.messageId, "Error" : task.error!.localizedDescription])
                        break
                    case .paused:
                         NotificationCenter.default.post(name: AWSNotificationFailed, object: ["messageId" : self.messageId, "Error" : "Paused"])
                        break
                    default:
                        _ = self.downloadTranferRequest?.cancel()
                    }
                } else {
                    _ = self.downloadTranferRequest?.cancel()
                     NotificationCenter.default.post(name: AWSNotificationFailed, object: ["messageId" : self.messageId, "Error" : task.error!.localizedDescription])
                }
               
                return nil
            }
            else
            {
                //success
                if let index = AWSUtilityManager.shared.downloadRequestArray.index(where: {$0.messageId == self.messageId}) {
                    AWSUtilityManager.shared.downloadRequestArray.remove(at: index)
                    AWSUtilityManager.shared.uploadingList.removeValue(forKey: self.messageId)
                }
            }
            
            return nil
        })
        
        self.downloadTranferRequest!.downloadProgress = {(bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) -> Void in
            DispatchQueue.main.async(execute: {() -> Void in
                //Update progress
                let percentDownload : CGFloat = CGFloat((totalBytesSent * 100) / totalBytesExpectedToSend)
                
                self.progressValue = CGFloat(percentDownload / 100)
                
                NotificationCenter.default.post(name: AWSNotificationProgress, object: ["messageId" : self.messageId, "Progress" : self.progressValue])
                
//                print("\nTotal Download in progress : \(AppInstance!.downloadRequestArray.count)")
//                print("\nMessage ID ======> \(self.messageId)")
//                print("\nPercentage download : \(percentDownload)")
            })
        }
    }
}
