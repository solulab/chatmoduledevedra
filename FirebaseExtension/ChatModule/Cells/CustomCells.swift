//
//  CustomCells.swift
//  FirebaseExtension
//
//  Created by User on 3/6/18.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class MediaCell : ChatUserCell
{
    @IBOutlet var progressView: GBKUIButtonProgressView!
    @IBOutlet var mediaImgView : UIImageView!
    @IBOutlet var btnVideoAction : UIButton!
    @IBOutlet var btnRetryAction : UIButton!
    @IBOutlet var spinnerView: MMMaterialDesignSpinner!
    
    var mediaURL = ""
    var playerDelegate : PlayerProtocol?
    
    var conversationID = ""
    var messageRef : DatabaseReference?
    
    var isDownloadCell : Bool = false
    
     func clearData()
     {
        mediaURL = ""
        mediaImgView.image = UIImage()
        self.btnRetryAction.isHighlighted = false
     }
    
    override func awakeFromNib()
    {
        self.progressView.addTarget(self, action: #selector(pauseTapped(sender:)), for: .touchUpInside)
    }
    
    @IBAction func btnVideoTapped(_ sender: Any)
    {
        if mediaURL.characters.count > 0
        {
            if Helper.isFileExistInDevice(nameOfFile:mediaURL)
            {
                playFile(nameOfFile :mediaURL)
            }
            else
            {
                if isDownloadCell{
                    downloadFile(nameOfFile : mediaURL)
                }else{
                    Helper.displayAlert(title: "Media not exist in your device", msg: "")
                  //  KSToastView.ks_showToast("Media not exist in your device", duration: 3)
                }
            }
        }
    }
    
    @IBAction func btnRetryTapped(_ sender: Any)
    {
        
        var dict : [String : String]?
        dict = AWSUtilityManager.shared.getURLDetailsFor(self.messageID)
        
        if dict != nil
        {
            let messageRef = self.messageRef?.child(self.conversationID).child(messageID)
            let filename = dict!["FileName"]
            
            self.progressView.setProgress(0.1, animated: true)
            if isDownloadCell{
                AWSUtilityManager.shared.downloadFileWith(filename!, messageID: self.messageID, messageRef: messageRef!)
                self.btnRetryAction.isHidden = true
                
            }else{
                let fileURL = URL(fileURLWithPath: dict!["FileURL"]!)
                
                if AWSUtilityManager.shared.uploadFileWith(filename!, fileURL: fileURL, key: self.messageID, messageRef: messageRef!) == false{
                    //File not found
                    //KSToastView.ks_showToast("File not found", duration: 3)
                }else{
                    self.btnRetryAction.isHidden = true
                }
            }
            
        }
        else
        {
            downloadFile(nameOfFile : mediaURL)
        }
    }
    
    func playFile(nameOfFile : String)
    {
        let videoURL = Helper.getFilePath(nameOfFile: nameOfFile)
        
        if let delegate = self.playerDelegate{
            delegate.playURL(url: videoURL)
        }
    }
    
    func downloadFile(nameOfFile : String)
    {
        if let delegate = self.playerDelegate{
            delegate.downloadURL(nameOfFile: nameOfFile,messageID: self.messageID, completion: { (isDownloaded) in
                if isDownloaded{
                    self.btnVideoAction.isHidden = false
                }
            })
        }
    }
    
    @objc func pauseTapped(sender : Any)
    {
        AWSUtilityManager.shared.pauseUploadWith(self.messageID, isForDownload: isDownloadCell)
        self.btnVideoAction.isHidden = true
        self.btnRetryAction.isHidden = false
        self.btnRetryAction.isHighlighted = true
        self.progressView.isHidden = true
    }
}

class ChatUserCell : UITableViewCell
{
    @IBOutlet var btnAdmin: UIButton!
    @IBOutlet var lblUserName : UILabel!
    @IBOutlet var lblMessage : UILabel!
    @IBOutlet var lblTime : UILabel!
    @IBOutlet var imgDP : UIImageView!
    @IBOutlet var btn : UIButton!
    @IBOutlet var msgBackground : UIView!
    @IBOutlet var txtviewMessage : UITextView!
    @IBOutlet var imgTick : UIImageView!
    @IBOutlet var btnAccept : UIButton!
    @IBOutlet var btnReject : UIButton!
    @IBOutlet var lblNameHeight: NSLayoutConstraint!
    @IBOutlet var lblUnReadMsgCount : UILabel!
    
    var delegate : ChatUserDetailsProtocol?
    var messageCountDelegate : MessageCountProtocol?
    var userListDelegate : UserListViewController?
    
    var currentChatUser = ChatUser()
    var currentGroup = ChatGroup()
    
    var cellData = Conversation()
    var isStatusChanged = false
    var messageID = ""
    
    func displayCellData()
    {
        if showTypingIndicator{
            checkUserStatus()
        }
        
        if let dele = self.messageCountDelegate
        {
           // if cellData.isValueChanged == false{
                dele.getUnreadMessagesCount(obj: cellData, completion: { (counter) in
                    
                    if counter > 0
                    {
                        self.lblUnReadMsgCount.isHidden = false
                        self.lblUnReadMsgCount.text = "\(counter)"
                    }else{
                        self.lblUnReadMsgCount.isHidden = true
                    }
                    
                })
           // }
        }
        
        if !isStatusChanged{
            lblMessage.text = cellData.lastMessage.messageData.messageText
        }
        
        if cellData.lastMessage.messageData.timestamp.characters.count > 0{
            let dateString = Helper.getFormattedDateTime(secondss: Double(cellData.lastMessage.messageData.timestamp)!, isChatDetailScreen: false)
            
            self.lblTime.text = dateString
        }
        else{
            self.lblTime.text = "-"
        }
    }
    
    func displayUserDetail(userID : String?)
    {
        delegate?.getUserNameForUserID(userID, completion: { (userObj) in
            self.lblUserName.text = userObj.name
            self.imgDP.sd_setImage(with: URL(string:userObj.imageURL), completed: nil)
            
            if let dele = self.userListDelegate
            {
                self.cellData.username = userObj.name
                self.cellData.userImage = userObj.imageURL
                //dele.addDataInLocalDB(converObj: self.cellData)
            }
        })
    }
    
    func checkUserStatus()
    {
        self.isStatusChanged = false
        self.lblMessage.textColor = UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1)
        self.lblMessage.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        
        for(key,value) in cellData.typingDictionary!
        {
            if String(describing: value) == "true" && String(describing: key) != kcurrentUSERID
            {
                self.isStatusChanged = true
                self.lblMessage.textColor = UIColor.green
                self.lblMessage.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
                if cellData.isGroup
                {
                    for(key,value) in cellData.typingDictionary!
                    {
                        if String(describing: value) == "true" && String(describing: key) != kcurrentUSERID
                        {
                            delegate?.getUserNameForUserID((key as! String), completion: { (userObj) in
                                self.lblMessage.text = "\(userObj.name) is typing..."
                            })
                        }
                        
                    }
                }else
                {
                    lblMessage.text = "typing..."
                }
            }
        }
    }
    
    func displayGroupDetail(conversationID : String?)
    {
        delegate?.getGroupNameForConvID(conversationID, completion:
            { (groupObj) in
            self.lblUserName.text = groupObj.name
            self.imgDP.sd_setImage(with: URL(string:groupObj.imageURL), completed: nil)
            
            if let dele = self.userListDelegate
            {
                self.cellData.username = groupObj.name
                self.cellData.userImage = groupObj.imageURL
//                dele.addDataInLocalDB(converObj: self.cellData)
            }
        })
    }
    
    
}

class ProfileHeaderCell : UITableViewCell
{
    @IBOutlet var lblTitle : UILabel!
}
